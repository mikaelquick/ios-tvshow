//
//  EpisodeView.swift
//  TvShow
//
//  Created by Mikael  Quick on 2017-10-11.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit

class EpisodeView: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var seasonLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var airDateLabel: UILabel!
    @IBOutlet weak var airTimeLabel: UILabel!
    
    @IBOutlet weak var episodePicture: UIImageView!
    var selectedEpisode : Episodes!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  selectedEpisode.image?.original != nil{
            episodePicture.image = MainView.linkToAImage(url: (selectedEpisode.image?.medium)!)
        }
        
        if selectedEpisode.name != nil {
            nameLabel.text = selectedEpisode.name
        }
        
        if selectedEpisode.season > 0{
            seasonLabel.text = "Season: \(selectedEpisode.season)"
        }
        
        if selectedEpisode.number > 0{
            numberLabel.text = "Number: \(selectedEpisode.number)"
        }
        
        if selectedEpisode.airdate != nil{
            airDateLabel.text = "Airdate: \(selectedEpisode.airdate!)"
        }
        
        if selectedEpisode.airtime != nil{
            airTimeLabel.text = "Airtime: \(selectedEpisode.airtime!)"
        }
    }
}
