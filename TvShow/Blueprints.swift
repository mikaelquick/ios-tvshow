//
//  JasonBlueprints.swift
//  TvShow
//
//  Created by Mikael  Quick on 2017-10-11.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import Foundation

struct SearchedShows: Decodable {
    
    let show: Show
    
    struct Show: Codable {
        let name: String
        let id : Int
        let type : String
        let status : String
        let runtime : Float?
        let officialSite : String?
        let image : Image?
        let language : String?
        let premiered : String?
        let rating : Rating
        
        struct Image: Codable {
            let medium : String?
            let original : String?
        }
        struct Rating: Codable {
            let average : Float?
        }
    }
}

struct Episodes : Codable{
    var name : String?
    var season : Int
    let number : Int
    let image : Image?
    let airdate : String?
    let airtime : String?
    
    struct Image: Codable {
        let medium : String?
        let original : String?
    }
}
