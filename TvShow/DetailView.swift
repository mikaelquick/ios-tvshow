//
//  DetailView.swift
//  TvShow
//
//  Created by Mikael  Quick on 2017-10-11.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit

class DetailView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var selectedEpisode : Episodes!
    var tempImage : UIImage!
    var decoded : [Episodes] = []
    var searchedShows : SearchedShows!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var premiered: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image.image = tempImage
        nameLabel.text = searchedShows?.show.name
        typLabel.text = "Type: \(searchedShows.show.type)"
        languageLabel.text = "Language: \(searchedShows.show.language!)"
        
        if(searchedShows.show.premiered != nil){
            premiered.text = "Premiered: \(searchedShows.show.premiered!)"
        }
        else{
            ratingLabel.text = "Premiered: Info missing"
        }
        
        if(searchedShows.show.rating.average != nil){
            ratingLabel.text = "Rating: \(searchedShows.show.rating.average!)"
        }
        else{
            ratingLabel.text = "Rating: Info missing"
        }
        getData()
    }
    
    func getData(){
        
        let url = NSURL(string: "http://api.tvmaze.com/shows/\(searchedShows.show.id)/episodes")
        
        let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
            
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
            
            DispatchQueue.main.async{
                self.decodeJson(mydata: data!)
                self.tableView.reloadData()
            }
        }
        task.resume()
    }
    
    func decodeJson(mydata : Data){
        let decoder = JSONDecoder()
        decoded = try! decoder.decode([Episodes].self, from: mydata)
    }
    
    //Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return decoded.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailCell
        cell.cellLabel.text = "Season:\(decoded[indexPath.row].season) \(decoded[indexPath.row].name!)"
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectedEpisodeClick(tapGestureRecognizer:)))
        cell.isUserInteractionEnabled = true
        cell.tag = indexPath.row
        
        cell.addGestureRecognizer(tapGestureRecognizer)
        
        return cell
    }
    
    @objc func selectedEpisodeClick(tapGestureRecognizer : UITapGestureRecognizer) {
        let number = tapGestureRecognizer.view?.tag
        selectedEpisode = decoded[number!]
        performSegue(withIdentifier: "episodeViewFromDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "episodeViewFromDetail"{
            let destination = segue.destination as! EpisodeView
            destination.selectedEpisode = selectedEpisode
        }
    }
    
}

class DetailCell : UITableViewCell{
    
    @IBOutlet weak var cellLabel: UILabel!
}
