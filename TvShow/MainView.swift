//
//  ViewController.swift
//  TvShow
//
//  Created by Mikael  Quick on 2017-10-10.
//  Copyright © 2017 MikaelQuick. All rights reserved.
//

import UIKit

class MainView: UIViewController , UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var chosenImage : UIImage!
    var searchedShows : SearchedShows?
    var decoded = [SearchedShows]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.keyboardDismissMode = .onDrag
    }
   
    func getData(searchWord: String){
        let url = NSURL(string: "http://api.tvmaze.com/search/shows?q=" + searchWord)
        let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
            
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
            
            DispatchQueue.main.async{
                self.decodeJson(mydata: data!)
                self.tableview.reloadData()
            }
        }
        
        task.resume()
    }
    
    func decodeJson(mydata : Data){
        let decoder = JSONDecoder()
        decoded = try! decoder.decode([SearchedShows].self, from: mydata)
        
    }
    
    static func linkToAImage(url : String) -> UIImage{
        let url = NSURL(string: url)
        let data = NSData(contentsOf:url! as URL)
        let imageView = UIImage(data: data! as Data)!
        
        return imageView
    }
    
    func search(){
        var searched = searchBar.text?.trimmingCharacters(in: .whitespaces)
        searched = searched!.replacingOccurrences(of: "[\\[\\]^+<>@£$∞§|≈]", with: "", options: .regularExpression, range: nil)
        searched = searched!.replacingOccurrences(of: "[ÅåÄä]", with: "A", options: .regularExpression, range: nil)
        searched = searched!.replacingOccurrences(of: "[Öö]", with: "O", options: .regularExpression, range: nil)
        searched = searched?.replacingOccurrences(of: " ", with: "%20")
        getData(searchWord: searched!)
    }
    
    //Clicking
    @IBAction func searchClick(_ sender: Any) {
        search()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
        search()
        
    }
    
    @objc func selectedTvShowClick(tapGestureRecognizer : UIGestureRecognizer ){
        let row = tapGestureRecognizer.view?.tag
        let myCell = (tapGestureRecognizer.view) as! MainViewTableCell
        chosenImage = myCell.cellImage.image!
        searchedShows = decoded[row!]
        performSegue(withIdentifier: "detailViewFromMain", sender: self)
    }
    
    //Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return decoded.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainViewTableCell
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectedTvShowClick(tapGestureRecognizer:)))
        cell.isUserInteractionEnabled = true
        
        cell.addGestureRecognizer(tapGestureRecognizer)
        cell.tag = indexPath.row
        
        if(decoded[indexPath.row].show.image?.original != nil){
            cell.cellImage.image = MainView.linkToAImage(url: (decoded[indexPath.row].show.image?.original)!)
        }
        return cell
    }
    
    //Before we leave this forsaken please
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailViewFromMain"{
            let destination = segue.destination as! DetailView
            destination.tempImage = chosenImage
            destination.searchedShows = searchedShows
        }
    }
}
class MainViewTableCell : UITableViewCell{
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
}


